#include "cuda_runtime.h"
#include "device_launch_parameters.h"

#include <stdio.h>
#include <memory.h>
#include <stdlib.h>
#include <time.h>

// helper functions
long timediff(clock_t t1, clock_t t2);

int main(int argc, char* argv[])
{
	int arraySize, iterations, isSerial;
	int *originArray;

	if (argc != 4) {
		printf("The Arguments you entered are wrong.\n");
		printf("./mem_alloc <arraySize> <iterations> <isSerial>\n");
		return EXIT_FAILURE;
	}
	else {
		arraySize = atoi(argv[1]);
		iterations = atoi(argv[2]);
		isSerial = atoi(argv[3]);
	}

	// time malloc
	clock_t timer_start, timer_end;

	// START TIMER
	timer_start = clock();

	for (int i = 0; i < iterations; i++) {
		if (isSerial == 1) {
			originArray = (int*)malloc(sizeof(int) * arraySize);
			free(originArray);
		}
		else {
			cudaMalloc(&originArray, sizeof(int) * arraySize);
			cudaFree(originArray);
		}
	}

	timer_end = clock();

	// Do the time calcuclation
	//PRINT TIMER
	printf("%ld ms\n", timediff(timer_start, timer_end));

	return EXIT_SUCCESS;
}

long timediff(clock_t t1, clock_t t2) {
	double elapsed;
	elapsed = ((double)t2 - t1) / CLOCKS_PER_SEC * 1000;
	return elapsed;
}