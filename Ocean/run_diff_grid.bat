echo off

echo "6 B 1000 It Serial"
..\..\x64\Debug\Ocean 6 6 2000 1 8 1 1

echo "6 B 1000 It Parallel"
..\..\x64\Debug\Ocean 6 6 2000 1 8 1 0

echo "10 B 1000 It Serial"
..\..\x64\Debug\Ocean 10 10 2000 1 32 1 1

echo "10 B 1000 It Parallel"
..\..\x64\Debug\Ocean 10 10 2000 1 32 1 0

echo "18 B 1000 It Serial"
..\..\x64\Debug\Ocean 18 18 2000 1 128 1 1

echo "18 B 1000 It Parallel"
..\..\x64\Debug\Ocean 18 18 2000 1 128 1 0

echo "34 B 1000 It Serial"
..\..\x64\Debug\Ocean 34 34 2000 1 512 1 1

echo "34 B 1000 It Parallel"
..\..\x64\Debug\Ocean 34 34 2000 1 512 1 0

echo "66 B 1000 It Serial"
..\..\x64\Debug\Ocean 66 66 2000 4 512 1 1

echo "66 B 1000 It Parallel"
..\..\x64\Debug\Ocean 66 66 2000 4 512 1 0

echo "130 B 1000 It Serial"
..\..\x64\Debug\Ocean 130 130 2000 16 512 1 1

echo "130 B 1000 It Parallel"
..\..\x64\Debug\Ocean 130 130 2000 16 512 1 0

echo "258 B 1000 It Serial"
..\..\x64\Debug\Ocean 258 258 2000 64 512 1 1

echo "258 B 1000 It Parallel"
..\..\x64\Debug\Ocean 258 258 2000 64 512 1 0

echo "514 B 1000 It Serial"
..\..\x64\Debug\Ocean 514 514 2000 256 512 1 1

echo "514 B 1000 It Parallel"
..\..\x64\Debug\Ocean 514 514 2000 256 512 1 0

echo "1026 B 1000 It Serial"
..\..\x64\Debug\Ocean 1026 1026 2000 1024 512 1 1

echo "1026 B 1000 It Parallel"
..\..\x64\Debug\Ocean 1026 1026 2000 1024 512 1 0

echo "2050 B 1000 It Serial"
..\..\x64\Debug\Ocean 2050 2050 2000 4096 512 1 1

echo "2050 B 1000 It Parallel"
..\..\x64\Debug\Ocean 2050 2050 2000 4096 512 1 0

echo "4096 B 1000 It Serial"
..\..\x64\Debug\Ocean 4096 4096 2000 16384 512 1 1

echo "4096 B 1000 It Parallel"
..\..\x64\Debug\Ocean 4096 4096 2000 16384 512 1 0

echo "8194 B 1000 It Serial"
..\..\x64\Debug\Ocean 8194 8194 2000 65535 512 1 1

echo "8194 B 1000 It Parallel"
..\..\x64\Debug\Ocean 8194 8194 2000 65535 512 1 0