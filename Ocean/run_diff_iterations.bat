echo off

echo "1026 B 1 It Serial"
..\..\x64\Debug\Ocean 1026 1026 2 1024 512 1 1

echo "1026 B 1 It Parallel"
..\..\x64\Debug\Ocean 1026 1026 2 1024 512 1 0

echo "1026 B 2 It Serial"
..\..\x64\Debug\Ocean 1026 1026 4 1024 512 1 1

echo "1026 B 2 It Parallel"
..\..\x64\Debug\Ocean 1026 1026 4 1024 512 1 0

echo "1026 B 4 It Serial"
..\..\x64\Debug\Ocean 1026 1026 8 1024 512 1 1

echo "1026 B 4 It Parallel"
..\..\x64\Debug\Ocean 1026 1026 8 1024 512 1 0

echo "1026 B 8 It Serial"
..\..\x64\Debug\Ocean 1026 1026 16 1024 512 1 1

echo "1026 B 8 It Parallel"
..\..\x64\Debug\Ocean 1026 1026 16 1024 512 1 0

echo "1026 B 16 It Serial"
..\..\x64\Debug\Ocean 1026 1026 32 1024 512 1 1

echo "1026 B 16 It Parallel"
..\..\x64\Debug\Ocean 1026 1026 32 1024 512 1 0

echo "1026 B 32 It Serial"
..\..\x64\Debug\Ocean 1026 1026 64 1024 512 1 1

echo "1026 B 32 It Parallel"
..\..\x64\Debug\Ocean 1026 1026 64 1024 512 1 0

echo "1026 B 64 It Serial"
..\..\x64\Debug\Ocean 1026 1026 128 1024 512 1 1

echo "1026 B 64 It Parallel"
..\..\x64\Debug\Ocean 1026 1026 128 1024 512 1 0

echo "1026 B 128 It Serial"
..\..\x64\Debug\Ocean 1026 1026 256 1024 512 1 1

echo "1026 B 128 It Parallel"
..\..\x64\Debug\Ocean 1026 1026 256 1024 512 1 0

echo "1026 B 256 It Serial"
..\..\x64\Debug\Ocean 1026 1026 512 1024 512 1 1

echo "1026 B 256 It Parallel"
..\..\x64\Debug\Ocean 1026 1026 512 1024 512 1 0

echo "1026 B 512 It Serial"
..\..\x64\Debug\Ocean 1026 1026 1024 1024 512 1 1

echo "1026 B 512 It Parallel"
..\..\x64\Debug\Ocean 1026 1026 1024 1024 512 1 0

echo "1026 B 1024 It Serial"
..\..\x64\Debug\Ocean 1026 1026 2048 1024 512 1 1

echo "1026 B 1024 It Parallel"
..\..\x64\Debug\Ocean 1026 1026 2048 1024 512 1 0

echo "1026 B 2048 It Serial"
..\..\x64\Debug\Ocean 1026 1026 4096 1024 512 1 1

echo "1026 B 2048 It Parallel"
..\..\x64\Debug\Ocean 1026 1026 4096 1024 512 1 0