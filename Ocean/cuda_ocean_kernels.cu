#ifdef VERSION1
__global__ void ocean_kernel(int *grid, int xdim, int ydim, int offset)
{
    int threads = gridDim.x*blockDim.x;
    int threadId  = blockDim.x*blockIdx.x + threadIdx.x;

    if (threads > (xdim-2)*(ydim-2)) {
        threads = (xdim-2)*(ydim-2);
        if (threadId >= threads) {
            return;
        }
    }

    int chunk = (xdim-2)*(ydim-2)/threads;
    int start = threadId * chunk;
    int end = (threadId + 1) * chunk;

    int threadsPerRow = (xdim - 2);

    for (int i=start; i<end; i++) {
        if (offset) {
            if (i%2) continue;
        } else {
            if (!(i%2)) continue;
        }

        int row = i / threadsPerRow;
        int col = i % threadsPerRow;

        int loc = xdim + row * xdim + col;
        if (offset) {
            loc += (row%2) ? 1 : 0;
            loc += 1;
        } else {
            loc += (row%2) ? 0 : 1;
        }
        // printf("Row: %d, Col: %d\n", row, col);
        // printf("loc: %d\n", loc);

        grid[loc] = (grid[loc]
                  + grid[loc - xdim]
                  + grid[loc + xdim]
                  + grid[loc + 1]
                  + grid[loc - 1])
                  / 5;
    }
}
#endif

#ifdef VERSION2
__global__ void ocean_kernel(int *grid, int xdim, int ydim, int offset)
{
	// figure out thread Id
	int numThreads = gridDim.x*blockDim.x;
	int threadId = (blockDim.x*blockIdx.x) + threadIdx.x;

	// if number of threads too large and threadId exceeds this, exit
	if (numThreads > ((xdim - 2)*(ydim - 2)) / 2) {
		numThreads = ((xdim - 2)*(ydim - 2)) / 2;
		if (threadId >= numThreads) {
			return;
		}
	}
	
	// need to figure out what row to ensure we get the right block
	int threadsPerRow = (xdim - 2) / 2;
	int row = threadId / threadsPerRow;
	int col = threadId % threadsPerRow;

	// based on thread Id, figure out what block to work on
	int index = xdim + (row * xdim) + (col * 2) + 1;

	if (offset) {
		index += (row % 2) ? 1 : 0;
	}
	else {
		index += (row % 2) ? 0 : 1;
	}

	// set grid
	grid[index] = (grid[index]
		+ grid[index - xdim]
		+ grid[index + xdim]
		+ grid[index + 1]
		+ grid[index - 1])
		/ 5;
}
#endif

#ifdef VERSION3

__global__ void split_array_kernel(int *grid, int *red_grid, int *black_grid, int xdim, int ydim)
{
    // This kernel should take the contents of grid and copy all of the red
    // elements into red_grid and all of the black elements into black_grid
}

__global__ void unsplit_array_kernel(int *grid, int *red_grid, int *black_grid, int xdim, int ydim)
{
    // This kernel should take the red_grid and black_grid and copy it back into grid
}

__global__ void ocean_kernel(int *red_grid, int *black_grid, int xdim, int ydim, int offset)
{
    // Your code for step 3
}
#endif
