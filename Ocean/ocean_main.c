#include <stdio.h>
#include <stdlib.h>
#include <time.h>

/* Implement this function in serial_ocean and omp_ocean */
extern void ocean_serial(int **grid, int xdim, int ydim, int timesteps);
extern void ocean (int** grid, int xdim, int ydim, int timesteps, int numBlocks, int numThreads);

// helper functions
void printGrid(int** grid, int xdim, int ydim);
long timediff(clock_t t1, clock_t t2);

int main(int argc, char* argv[])
{
    int xdim,ydim,timesteps;
    int** grid;
    int i,j,t;
	int numBlocks, numThreads;
	int isSerial, isRand;

    /********************Get the arguments correctly (start) **************************/
    /*
    Three input Arguments to the program
    1. X Dimension of the grid
    2. Y dimension of the grid
    3. number of timesteps the algorithm is to be performed
    */

    if (argc!=8) {
		printf("The Arguments you entered are wrong.\n");
        printf("./ocean <x-dim> <y-dim> <timesteps> <numBlocks> <numThreads> <rand> <isSerial>\n");
        return EXIT_FAILURE;
    } else {
        xdim = atoi(argv[1]);
        ydim = atoi(argv[2]);
        timesteps = atoi(argv[3]);
		numBlocks = atoi(argv[4]);
		numThreads = atoi(argv[5]);
		isRand = atoi(argv[6]);
		isSerial = atoi(argv[7]);
    }
    ///////////////////////Get the arguments correctly (end) //////////////////////////


    /*********************create the grid as required (start) ************************/
    /*
    The grid needs to be allocated as per the input arguments and randomly initialized.
    Remember during allocation that we want to gaurentee a contiguous block, hence the
    nasty pointer math.

    To test your code for correctness please comment this section of random initialization.
    */
    grid = (int**) malloc(ydim*sizeof(int*));
    int *temp = (int*) malloc(xdim*ydim*sizeof(int));
	int *temp_serial = (int*)malloc(xdim*ydim*sizeof(int));

    for (i=0; i<ydim; i++) {
        grid[i] = &temp[i*xdim];
    }
    for (i=0; i<ydim; i++) {
        for (j=0; j<xdim; j++) {
			if (isRand == 1) {
				int randNum = rand();
				grid[i][j] = randNum;
			}
			else {
				if (i == 0 || j == 0 || i == ydim - 1 || j == xdim - 1) {
					grid[i][j] = 100;
				}
				else {
					grid[i][j] = 0;
				}
			}
            
        }
    }
    ///////////////////////create the grid as required (end) //////////////////////////
	
	
	clock_t timer_start, timer_end;

    // START TIMER
	timer_start = clock();
	if (isSerial == 1) {
		ocean_serial(grid, xdim, ydim, timesteps);
	}
	else {
		ocean(grid, xdim, ydim, timesteps, numBlocks, numThreads);
	}

	timer_end = clock();

    // Do the time calcuclation
    //PRINT TIMER
	printf("Total Execution time: %ld ms\n", timediff(timer_start, timer_end));

    //printGrid(grid, xdim, ydim);

    // Free the memory we allocated for grid
    free(temp);
    free(grid);

    return EXIT_SUCCESS;
}

long timediff(clock_t t1, clock_t t2) {
	long elapsed;
	elapsed = ((double)t2 - t1) / CLOCKS_PER_SEC * 1000;
	return elapsed;
}

void printGrid(int** grid, int xdim, int ydim)
{
    for (int i=1; i<xdim-1; i++) {
        for (int j=1; j<ydim-1; j++) {
            printf("%05d  ", grid[i][j]);
        }
        printf("\n");
    }
}
