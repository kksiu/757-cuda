
#include "cuda_runtime.h"
#include "device_launch_parameters.h"

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <memory>

void printGrid(int* grid, int xdim, int ydim);
void printArrays(double* x2, double* y2, int arraySize);
long timediff(clock_t t1, clock_t t2);

void arrayDFTSerial(double *x2, double* y2, double *x1, double *y1, int arraySize) {
	//iterate through each cell for every timestep
	for (int i = 0; i < arraySize; i++) {
		x2[i] = 0;
		y2[i] = 0;

		double arg = -2.0 * 3.141592654 * (double)i / (double)arraySize;

		for (int j = 0; j < arraySize; j++) {
			double cosarg = cos(j * arg);
			double sinarg = sin(j * arg);
			x2[i] += ((x1[j] * cosarg) - (y1[j] * sinarg));
			y2[i] += ((x1[j] * sinarg) + (y1[j] * cosarg));
		}

		x2[i] = x2[i] / (double)arraySize;
		y2[i] = y2[i] / (double)arraySize;
	}
}

__global__ void arrayDFT(double *x2, double* y2, double *x1, double *y1, int arraySize)
{
	// figure out total number of threads
	int numThreads = (blockDim.x * blockDim.y * gridDim.x * gridDim.y);

	// figure out the threadId
	int threadsPerBlock = (blockDim.x * blockDim.y);
	int threadId = (gridDim.x * threadsPerBlock * blockIdx.y) + (threadsPerBlock * blockIdx.x) + (blockDim.x * threadIdx.y) + threadIdx.x;

	// remove threads that go over the limit
	if (numThreads >= arraySize) {
		numThreads = arraySize;
		if (threadId >= arraySize) {
			return;
		}
	}

	// figure out start and end point for both x and y dimensions
	x2[threadId] = 0;
	y2[threadId] = 0;

	double arg = -2.0 * 3.141592654 * (double)threadId / (double)arraySize;

	for (int j = 0; j < arraySize; j++) {
		double cosarg = cos(j * arg);
		double sinarg = sin(j * arg);
		x2[threadId] += ((x1[j] * cosarg) - (y1[j] * sinarg));
		y2[threadId] += ((x1[j] * sinarg) + (y1[j] * cosarg));
	}

	x2[threadId] = x2[threadId] / (double)arraySize;
	y2[threadId] = y2[threadId] / (double)arraySize;
}

int main(int argc, char *argv[])
{
	// host variables
	int arraySize, timesteps;
	double* x1;
	double* y1;
	double* x2;
	double* y2;
	int doRand, isSerial;
	int blockXSize, blockYSize, threadXSize, threadYSize;

	// device variables
	double* x1_d;
	double* y1_d;
	double* x2_d;
	double* y2_d;

	if (argc != 9) {
		printf("The Arguments you entered are wrong.\n");
		printf("./dft <arraySize> <blockXSize> <blockYSize> <threadXSize> <threadYSize> <timesteps> <random> <isSerial>\n");
		return EXIT_FAILURE;
	}
	else {
		arraySize = atoi(argv[1]);
		blockXSize = atoi(argv[2]);
		blockYSize = atoi(argv[3]);
		threadXSize = atoi(argv[4]);
		threadYSize = atoi(argv[5]);
		timesteps = atoi(argv[6]);
		doRand = atoi(argv[7]);
		isSerial = atoi(argv[8]);
	}

	x1 = (double*)malloc(arraySize * sizeof(double));
	y1 = (double*)malloc(arraySize * sizeof(double));
	x2 = (double*)malloc(arraySize * sizeof(double));
	y2 = (double*)malloc(arraySize * sizeof(double));

	for (int i = 0; i < arraySize; i++) {
		if (doRand) {
			x1[i] = ((double)rand() / (double)RAND_MAX);
			y1[i] = ((double)rand() / (double)RAND_MAX);
		}
		else {
			x1[i] = (double)i + 1;
			y1[i] = (double)i + 1;
		}
	}

	clock_t timer_start, timer_end;

	// START TIMER
	timer_start = clock();

	if (isSerial == 1) {
		for (int i = 0; i < timesteps; i++) {
			arrayDFTSerial(x2, y2, x1, y1, arraySize);
			memcpy(x1, x2, arraySize * sizeof(double));
			memcpy(y1, y2, arraySize * sizeof(double));
		}
	}
	else {
		// allocate CUDA memory
		cudaMalloc(&x1_d, arraySize * sizeof(double));
		cudaMalloc(&y1_d, arraySize * sizeof(double));
		cudaMalloc(&x2_d, arraySize * sizeof(double));
		cudaMalloc(&y2_d, arraySize * sizeof(double));

		// move memory over to CUDA
		cudaMemcpy(x1_d, x1, arraySize * sizeof(double), cudaMemcpyHostToDevice);
		cudaMemcpy(y1_d, y1, arraySize * sizeof(double), cudaMemcpyHostToDevice);

		// call matrix function
		// Add vectors in parallel.
		for (int i = 0; i < timesteps; i++) {
			dim3 gridDim(blockXSize, blockYSize, 1);
			dim3 blockDim(threadXSize, threadYSize, 1);
			arrayDFT << <gridDim, blockDim >> >(x2_d, y2_d, x1_d, y1_d, arraySize);
			cudaMemcpy(x1_d, x2_d, arraySize * sizeof(double), cudaMemcpyDeviceToDevice);
			cudaMemcpy(y1_d, y2_d, arraySize * sizeof(double), cudaMemcpyDeviceToDevice);
		}

		cudaMemcpy(x2, x2_d, arraySize * sizeof(double), cudaMemcpyDeviceToHost);
		cudaMemcpy(y2, y2_d, arraySize * sizeof(double), cudaMemcpyDeviceToHost);
	}

	timer_end = clock();

	// Do the time calcuclation
	//PRINT TIMER
	printf("%ld ms\n", timediff(timer_start, timer_end));
	//printArrays(x2, y2, arraySize);

	// free host variables
	free(x1);
	free(y1);
	free(x2);
	free(y2);

	if (isSerial == 0) {
		cudaFree(x1_d);
		cudaFree(y1_d);
		cudaFree(x2_d);
		cudaFree(y2_d);
	}

	return 0;
}

void printArrays(double* x2, double* y2, int arraySize)
{
	printf("[");
	for (int i = 0; i < arraySize; i++) {
		if (i == arraySize - 1) {
			printf("%f + %fi]\n", x2[i], y2[i]);
		}
		else {
			printf("%f + %fi, ", x2[i], y2[i]);
		}
	}
}

long timediff(clock_t t1, clock_t t2) {
	long elapsed;
	elapsed = ((double)t2 - t1) / CLOCKS_PER_SEC * 1000;
	return elapsed;
}