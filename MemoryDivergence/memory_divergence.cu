#include "cuda_runtime.h"
#include "device_launch_parameters.h"

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <curand.h>
#include <curand_kernel.h>

void printArray(int* arr, int arraySize);
long timediff(clock_t t1, clock_t t2);

void serial_memory_divergence(int* destination, int* source, int arraySize)
{
	for (int i = 0; i < arraySize; i++) {
		destination[i] = source[rand() % arraySize];
	}
}

__global__ void memory_divergence(int* destination, int* source, int arraySize)
{

	curandState_t state;

	int numThreads = gridDim.x*blockDim.x;
	int threadId = (blockDim.x*blockIdx.x) + threadIdx.x;

	curand_init(threadId, /* the seed controls the sequence of random values that are produced */
		threadId, /* the sequence number is only important with multiple cores */
		arraySize, /* the offset is how much extra we advance in the sequence for each call, can be 0 */
		&state);

	if ((numThreads >= arraySize) && (threadId >= arraySize)) {
		return;
	}

	destination[threadId] = source[curand(&state) % arraySize];
}

int main(int argc, char *argv[])
{
	// host variables
	int arraySize, timesteps;
	int *source, *destination;
	int blockSize, threadSize, isRand, isSerial;

	// device variables
	int *d_source, *d_destination;

	if (argc != 7) {
		printf("The Arguments you entered are wrong.\n");
		printf("./branch_divergence <arraySize> <blockSize> <threadSize> <timesteps> <isRand> <isSerial>\n");
		return EXIT_FAILURE;
	}
	else {
		arraySize = atoi(argv[1]);
		blockSize = atoi(argv[2]);
		threadSize = atoi(argv[3]);
		timesteps = atoi(argv[4]);
		isRand = atoi(argv[5]);
		isSerial = atoi(argv[6]);
	}

	// allocate both arrays
	source = (int*)malloc(arraySize * sizeof(int));
	destination = (int*)malloc(arraySize * sizeof(int));

	// initialize
	for (int i = 0; i < arraySize; i++) {
		if (isRand == 0) {
			source[i] = i;
		}
		else {
			source[i] = rand();
		}
	}

	clock_t timer_start, timer_end;

	// START TIMER
	timer_start = clock();

	if (isSerial == 1) {
		for (int i = 0; i < timesteps; i++) {
			serial_memory_divergence(destination, source, arraySize);
		}
	}
	else {
		// cuda malloc
		cudaMalloc(&d_source, arraySize * sizeof(int));
		cudaMalloc(&d_destination, arraySize * sizeof(int));

		cudaMemcpy(d_source, source, arraySize * sizeof(int), cudaMemcpyHostToDevice);
		for (int i = 0; i < timesteps; i++) {
			memory_divergence << <blockSize, threadSize >> >(d_destination, d_source, arraySize);
		}
		cudaMemcpy(destination, d_destination, arraySize*sizeof(int), cudaMemcpyDeviceToHost);
	}

	timer_end = clock();

	// Do the time calcuclation
	//PRINT TIMER
	printf("%ld ms\n", timediff(timer_start, timer_end));
	//printArray(destination, arraySize);

	// Free the memory we allocated for array
	free(source);
	free(destination);

	if (isSerial == 0) {
		cudaFree(d_source);
		cudaFree(d_destination);
	}

	return EXIT_SUCCESS;
}

void printArray(int* arr, int arraySize) {
	printf("[");
	for (int i = 0; i < arraySize; i++) {
		if (i == arraySize - 1) {
			printf("%i]\n", arr[i]);
		}
		else {
			printf("%i, ", arr[i]);
		}
	}
}

long timediff(clock_t t1, clock_t t2) {
	long elapsed;
	elapsed = ((double)t2 - t1) / CLOCKS_PER_SEC * 1000;
	return elapsed;
}