#include "cuda_runtime.h"
#include "device_launch_parameters.h"

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

__global__ void ocean_kernel(int *grid, int xdim, int ydim, int timestep)
{
	for (int i = 1; i<xdim - 1; i++) {
		int offset = (i + timestep) % 2;
		for (int j = 1 + offset; j < ydim - 1; j += 2) {
			grid[(i * xdim) + j] = (grid[(i * xdim) + j] + grid[((i - 1) * xdim) + j] + grid[((i + 1) * xdim) + j]
				+ grid[(i * xdim) + j - 1] + grid[(i * xdim) + j + 1]) / 5;
		}
	}
}

void ocean_serial(int **grid, int xdim, int ydim, int timesteps)
{
	for (int ts = 0; ts<timesteps; ts++) {
		for (int i = 1; i<xdim - 1; i++) {
			int offset = (i + ts) % 2;
			for (int j = 1 + offset; j<ydim - 1; j += 2) {
				grid[i][j] = (grid[i][j] + grid[i - 1][j] + grid[i + 1][j]
					+ grid[i][j - 1] + grid[i][j + 1]) / 5;
			}
		}
	}
}

extern "C"
void ocean(int **grid, int xdim, int ydim, int timesteps)
{
	int *d_grid;

	cudaMalloc(&d_grid, sizeof(int)*xdim*ydim);

	cudaMemcpy(d_grid, &grid[0][0], xdim*ydim*sizeof(int), cudaMemcpyHostToDevice);

	for (int ts = 0; ts < timesteps; ts++) {
		ocean_kernel<<<1, 1>>>(d_grid, xdim, ydim, ts);
	}

	cudaMemcpy(&grid[0][0], d_grid, xdim*ydim*sizeof(int), cudaMemcpyDeviceToHost);

	cudaFree(d_grid);
}

// helper functions
void printGrid(int** grid, int xdim, int ydim);
long timediff(clock_t t1, clock_t t2);

int main(int argc, char* argv[])
{
	int xdim, ydim, timesteps;
	int** grid;
	int i, j, t;
	int isRand, isSerial;

	/********************Get the arguments correctly (start) **************************/
	/*
	Three input Arguments to the program
	1. X Dimension of the grid
	2. Y dimension of the grid
	3. number of timesteps the algorithm is to be performed
	*/

	if (argc != 6) {
		printf("The Arguments you entered are wrong.\n");
		printf("./serial_ocean <x-dim> <y-dim> <timesteps> <rand> <isSerial>\n");
		return EXIT_FAILURE;
	}
	else {
		xdim = atoi(argv[1]);
		ydim = atoi(argv[2]);
		timesteps = atoi(argv[3]);
		isRand = atoi(argv[4]);
		isSerial = atoi(argv[5]);
	}
	///////////////////////Get the arguments correctly (end) //////////////////////////


	/*********************create the grid as required (start) ************************/
	/*
	The grid needs to be allocated as per the input arguments and randomly initialized.
	Remember during allocation that we want to gaurentee a contiguous block, hence the
	nasty pointer math.

	To test your code for correctness please comment this section of random initialization.
	*/
	grid = (int**)malloc(ydim*sizeof(int*));
	int *temp = (int*)malloc(xdim*ydim*sizeof(int));
	int *temp_serial = (int*)malloc(xdim*ydim*sizeof(int));

	for (i = 0; i<ydim; i++) {
		grid[i] = &temp[i*xdim];
	}
	for (i = 0; i<ydim; i++) {
		for (j = 0; j<xdim; j++) {
			if (isRand == 1) {
				int randNum = rand();
				grid[i][j] = randNum;
			}
			else {
				if (i == 0 || j == 0 || i == ydim - 1 || j == xdim - 1) {
					grid[i][j] = 100;
				}
				else {
					grid[i][j] = 0;
				}
			}

		}
	}
	///////////////////////create the grid as required (end) //////////////////////////


	clock_t timer_start, timer_end;

	// START TIMER
	timer_start = clock();

	if (isSerial == 1) {
		ocean_serial(grid, xdim, ydim, timesteps);
	}
	else {
		ocean(grid, xdim, ydim, timesteps);
	}

	timer_end = clock();

	// Do the time calcuclation
	//PRINT TIMER
	printf("%ld ms\n", timediff(timer_start, timer_end));

	//printGrid(grid, xdim, ydim);

	// Free the memory we allocated for grid
	free(temp);
	free(grid);

	return EXIT_SUCCESS;
}

long timediff(clock_t t1, clock_t t2) {
	long elapsed;
	elapsed = ((double)t2 - t1) / CLOCKS_PER_SEC * 1000;
	return elapsed;
}

void printGrid(int** grid, int xdim, int ydim)
{
	for (int i = 1; i<xdim - 1; i++) {
		for (int j = 1; j<ydim - 1; j++) {
			printf("%05d  ", grid[i][j]);
		}
		printf("\n");
	}
}
