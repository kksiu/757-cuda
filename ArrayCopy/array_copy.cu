#include "cuda_runtime.h"
#include "device_launch_parameters.h"

#include <stdio.h>
#include <memory.h>
#include <stdlib.h>
#include <time.h>

// helper functions
void printArray(int* arr, int arraySize);
long timediff(clock_t t1, clock_t t2);

int main(int argc, char* argv[])
{
	int arraySize, isRand, isSerial;
	int *originArray;
	int *destinationArray;
	int *d_originArray;

	if (argc != 4) {
		printf("The Arguments you entered are wrong.\n");
		printf("./array_copy <arraySize> <isRand> <isSerial>\n");
		return EXIT_FAILURE;
	}
	else {
		arraySize = atoi(argv[1]);
		isRand = atoi(argv[2]);
		isSerial = atoi(argv[3]);
	}

	// malloc
	originArray = (int*) malloc(sizeof(int) * arraySize);
	destinationArray = (int*) malloc(sizeof(int) * arraySize);
	cudaMalloc(&d_originArray, sizeof(int) * arraySize);

	for (int i = 0; i < arraySize; i++) {
		if (isRand == 0) {
			originArray[i] = i;
		}
		else {
			originArray[i] = rand();
		}
	}

	clock_t timer_start, timer_end;

	// START TIMER
	timer_start = clock();

	if (isSerial == 1) {
		memcpy(destinationArray, originArray, sizeof(int) * arraySize);
	}
	else {
		cudaMemcpy(d_originArray, originArray, sizeof(int) * arraySize, cudaMemcpyHostToDevice);
		cudaMemcpy(destinationArray, d_originArray, sizeof(int) * arraySize, cudaMemcpyDeviceToHost);
	}

	timer_end = clock();

	// Do the time calcuclation
	//PRINT TIMER
	printf("%ld ms\n", timediff(timer_start, timer_end));
	//printArray(destinationArray, arraySize);

	// Free the memory we allocated for grid
	free(originArray);
	free(destinationArray);
	cudaFree(d_originArray);

	return EXIT_SUCCESS;
}

void printArray(int* arr, int arraySize) {
	printf("[");
	for (int i = 0; i < arraySize; i++) {
		if (i == arraySize - 1) {
			printf("%i]\n", arr[i]);
		}
		else {
			printf("%i, ", arr[i]);
		}
	}
}

long timediff(clock_t t1, clock_t t2) {
	long elapsed;
	elapsed = ((double)t2 - t1) / CLOCKS_PER_SEC * 1000;
	return elapsed;
}