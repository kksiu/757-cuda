
#include "cuda_runtime.h"
#include "device_launch_parameters.h"

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <memory>

void printGrid(int* grid, int xdim, int ydim);
void printArray(int* arr, int arraySize);
long timediff(clock_t t1, clock_t t2);

void matrixAddSerial(int *result, int **grid[2], int xdim, int ydim) {
	//iterate through each cell for every timestep
	for (int j = 0; j < xdim; j = j + 1) {
		for (int k = 0; k < ydim; k = k + 1) {
			result[(xdim * j) + k] = grid[0][j][k] + grid[1][j][k];
		}
	}
}

__global__ void matrixAdd(int* result_d, int* matrix0, int* matrix1, int xdim, int ydim)
{
	// figure out total number of threads
	int numThreads = (blockDim.x * blockDim.y * gridDim.x * gridDim.y);

	// figure out the threadId
	int threadsPerBlock = (blockDim.x * blockDim.y);
	int threadId = (gridDim.x * threadsPerBlock * blockIdx.y) + (threadsPerBlock * blockIdx.x) + (blockDim.x * threadIdx.y) + threadIdx.x;

	int matrixSize = xdim * ydim;

	// remove threads that go over the limit
	if (numThreads >= matrixSize) {
		numThreads = matrixSize;
		if (threadId >= matrixSize) {
			return;
		}
	}

	// figure out start and end point for both x and y dimensions
	int j = threadId % xdim;
	int i = threadId / xdim;

	result_d[(xdim * i) + j] = matrix0[(xdim * i) + j] + matrix1[(xdim * i) + j];

}

int main(int argc, char *argv[])
{
	// host variables
	int matrixSize, timesteps;
	int **matrix[2];
	int doRand, isSerial;
	int blockXSize, blockYSize, threadXSize, threadYSize;
	int *result;

	// device variables
	int *matrix_d[2];

	if (argc != 9) {
		printf("The Arguments you entered are wrong.\n");
		printf("./matrix_add <matrixSize> <blockXSize> <blockYSize> <threadXSize> <threadYSize> <timesteps> <random> <isSerial>\n");
		return EXIT_FAILURE;
	}
	else {
		matrixSize = atoi(argv[1]);
		blockXSize = atoi(argv[2]);
		blockYSize = atoi(argv[3]);
		threadXSize = atoi(argv[4]);
		threadYSize = atoi(argv[5]);
		timesteps = atoi(argv[6]);
		doRand = atoi(argv[7]);
		isSerial = atoi(argv[8]);
	}

	int fullMatrixSize = matrixSize * matrixSize;

	result = (int*)malloc(fullMatrixSize * sizeof(int));
	matrix[0] = (int **)malloc(matrixSize * sizeof(int *));
	matrix[1] = (int **)malloc(matrixSize * sizeof(int *));
	int *temp = (int *)malloc(fullMatrixSize * sizeof(int));
	int *other_temp = (int *)malloc(fullMatrixSize * sizeof(int));

	for (int i = 0; i < matrixSize; i++) {
		matrix[0][i] = &temp[i * matrixSize];
		matrix[1][i] = &other_temp[i * matrixSize];
	}
	for (int i = 0; i < matrixSize; i++) {
		for (int j = 0; j < matrixSize; j++) {
			if (doRand) {
				matrix[0][i][j] = rand() % 1000000;
				matrix[1][i][j] = rand() % 1000000;
			}
			else {
				matrix[0][j][i] = 100;
				matrix[1][j][i] = 100;
			}

		}
	}

	clock_t timer_start, timer_end;

	// START TIMER
	timer_start = clock();

	if (isSerial == 1) {
		for (int i = 0; i < timesteps; i++) {
			result = (((timesteps - 1) % 2) == 0) ? &matrix[0][0][0] : &matrix[1][0][0];
			matrixAddSerial(result, matrix, matrixSize, matrixSize);
		}
	}
	else {
		// allocate CUDA memory
		cudaMalloc(&matrix_d[0], fullMatrixSize * sizeof(int));
		cudaMalloc(&matrix_d[1], fullMatrixSize * sizeof(int));

		// move memory over to CUDA
		cudaMemcpy(matrix_d[0], &matrix[0][0][0], fullMatrixSize * sizeof(int), cudaMemcpyHostToDevice);
		cudaMemcpy(matrix_d[1], &matrix[1][0][0], fullMatrixSize * sizeof(int), cudaMemcpyHostToDevice);

		// call matrix function
		// Add vectors in parallel.
		int* result_d;
		for (int i = 0; i < timesteps; i++) {
			dim3 gridDim(blockXSize, blockYSize, 1);
			dim3 blockDim(threadXSize, threadYSize, 1);
			result_d = (((timesteps - 1) % 2) == 0) ? matrix_d[0] : matrix_d[1];
			matrixAdd<<<gridDim, blockDim>>>(result_d, matrix_d[0], matrix_d[1], matrixSize, matrixSize);
		}

		cudaMemcpy(result, result_d, matrixSize * matrixSize * sizeof(int), cudaMemcpyDeviceToHost);
	}

	timer_end = clock();

	// Do the time calcuclation
	//PRINT TIMER
	printf("%ld ms\n", timediff(timer_start, timer_end));
	//printf("%i\n", result[1000]);
	//printf("%i\n", result[100]);
	//printGrid(result, matrixSize, matrixSize);

	// free host variables
	free(temp);
	free(other_temp);
	free(matrix[0]);
	free(matrix[1]);

	if (isSerial == 0) {
		cudaFree(matrix_d[0]);
		cudaFree(matrix_d[1]);
	}

	return 0;
}

void printGrid(int* grid, int xdim, int ydim)
{
	for (int i = 0; i<xdim; i++) {
		for (int j = 0; j<ydim; j++) {
			printf("%05d  ", grid[(xdim * i) + j]);
		}
		printf("\n");
	}
}

long timediff(clock_t t1, clock_t t2) {
	long elapsed;
	elapsed = ((double)t2 - t1) / CLOCKS_PER_SEC * 1000;
	return elapsed;
}