#include "cuda_runtime.h"
#include "device_launch_parameters.h"

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <curand.h>
#include <curand_kernel.h>

int choosePathSerial(int maxBranches);
__device__ int choosePath(int maxBranches, curandState_t state);
void printArray(int* arr, int arraySize);
long timediff(clock_t t1, clock_t t2);

void serial_branch_divergence(int* arr, int arraySize, int maxBranches)
{
	for (int i = 0; i < arraySize; i++) {
		arr[i] = choosePathSerial(maxBranches);
	}
}

__global__ void branch_divergence(int* arr, int arraySize, int maxBranches)
{
	int numThreads = gridDim.x*blockDim.x;
	int threadId = (blockDim.x*blockIdx.x) + threadIdx.x;

	if ((numThreads >= arraySize) && (threadId >= arraySize)) {
		return;
	}

	curandState_t state;

	curand_init(threadId, /* the seed controls the sequence of random values that are produced */
		threadId, /* the sequence number is only important with multiple cores */
		arraySize, /* the offset is how much extra we advance in the sequence for each call, can be 0 */
		&state);

	arr[threadId] = choosePath(maxBranches, state);
}

int main(int argc, char *argv[])
{
	// host variables
	int arraySize, timesteps;
	int *arr;
	int blockSize, threadSize, isSerial;

	// device variables
	int *d_arr;

	if (argc != 6) {
		printf("The Arguments you entered are wrong.\n");
		printf("./branch_divergence <arraySize> <blockSize> <threadSize> <timesteps> <isSerial>\n");
		return EXIT_FAILURE;
	}
	else {
		arraySize = atoi(argv[1]);
		blockSize = atoi(argv[2]);
		threadSize = atoi(argv[3]);
		timesteps = atoi(argv[4]);
		isSerial = atoi(argv[5]);
	}

	// allocate both arrays
	arr = (int*)malloc(arraySize * sizeof(int));

	clock_t timer_start, timer_end;

	//max branches
	int maxBranches = 20;

	// START TIMER
	timer_start = clock();

	if (isSerial == 1) {
		for (int i = 0; i < timesteps; i++) {
			serial_branch_divergence(arr, arraySize, maxBranches);
		}
	}
	else {
		cudaMalloc(&d_arr, arraySize * sizeof(int));
		for (int i = 0; i < timesteps; i++) {
			branch_divergence <<<blockSize, threadSize>>>(d_arr, arraySize, maxBranches);
		}

		cudaMemcpy(arr, d_arr, arraySize*sizeof(int), cudaMemcpyDeviceToHost);
	}

	timer_end = clock();

	// Do the time calcuclation
	//PRINT TIMER
	printf("%ld ms\n", timediff(timer_start, timer_end));
	//printArray(arr, arraySize);

	// Free the memory we allocated for array
	free(arr);

	if (isSerial == 0) {
		cudaFree(d_arr);
	}
	

	return EXIT_SUCCESS;
}

void printArray(int* arr, int arraySize) {
	printf("[");
	for (int i = 0; i < arraySize; i++) {
		if (i == arraySize - 1) {
			printf("%i]\n", arr[i]);
		}
		else {
			printf("%i, ", arr[i]);
		}
	}
}

long timediff(clock_t t1, clock_t t2) {
	long elapsed;
	elapsed = ((double)t2 - t1) / CLOCKS_PER_SEC * 1000;
	return elapsed;
}

int choosePathSerial(int maxBranches) {
	int path = rand() % maxBranches;
	switch (path){
	case 0:
		return 0;
	case 1:
		return 123;
	case 2:
		return 154;
	case 3:
		return 103;
	case 4:
		return 513;
	case 5:
		return 65189;
	case 6:
		return -1981;
	case 7:
		return 5569151;
	case 8:
		return -661218;
	case 9:
		return 251;
	case 10:
		return 21684;
	case 11:
		return -9991;
	case 12:
		return 565498;
	case 13:
		return 1356914;
	case 14:
		return 64 / 41;
	case 15:
		return -15614999;
	case 16:
		return 54110;
	case 17:
		return 5;
	case 18:
		return 9581652;
	case 19:
		return 98412211178;
	}
	return -1;
}

__device__ int choosePath(int maxBranches, curandState_t state) {
	int path = curand(&state) % maxBranches;
	switch (path){
	case 0:
		return 0;
	case 1:
		return 123;
	case 2:
		return 154;
	case 3:
		return 103;
	case 4:
		return 513;
	case 5:
		return 65189;
	case 6:
		return -1981;
	case 7:
		return 5569151;
	case 8:
		return -661218;
	case 9:
		return 251;
	case 10:
		return 21684;
	case 11:
		return -9991;
	case 12:
		return 565498;
	case 13:
		return 1356914;
	case 14:
		return 64 / 41;
	case 15:
		return -15614999;
	case 16:
		return 54110;
	case 17:
		return 5;
	case 18:
		return 9581652;
	case 19:
		return 98412211178;
	}
	return -1;
}